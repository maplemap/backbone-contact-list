import Templates from '../templates';
import Events from '../events/';


export default class UsersView extends Backbone.View {
    tagName() { return "div"; }

    className() { return "contact";}

    events() {
        return {
            "click": "activeUserProfile"
        };
    }

    initialize() {
    }

    render() {
        this.$el.html( Templates.userListItem(this.model.toJSON()) );
        this.$el.attr('data-id', this.model.get('id'));

        return this;
    }

    activeUserProfile(e) {
        const userID = e.currentTarget.getAttribute('data-id');

        Events.trigger('show-user-profile', userID);
    }
}
