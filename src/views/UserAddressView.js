import Templates from '../templates/';


export default class UserAddressView extends Backbone.View {
    tagName() { return "div"; }

    className() { return "user-profile__address";}

    initialize(options) {
        this.template = Templates.userAddress(options.data)

        this.render();
    }

    render() {
        this.$el.html( this.template );

        return this;
    }
}
