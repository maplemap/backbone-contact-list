import Events from '../events';
import Templates from '../templates';
import UserProfileView from './UserProfileView';
import UserModel from '../models/UserModel';


export default class UsersProfilePageView extends Backbone.View {
    tagName() { return "div"; }

    className() { return "user-profile-page";}

    events() {
        return {
            "click .user-profile-page__close": "hideUserProfilePage"
        };
    }

    initialize() {
        this.userProfilePageTmp = Templates.userProfilePage();
        Events.on('show-user-profile', this.showUserProfilePage, this);

        this.render();
    }

    render() {
        this.$el.append( this.userProfilePageTmp );
        this.$closeBtn = this.$el.find(`.${this.className()}__close`);
        this.$content = this.$el.find(`.${this.className()}__content`);

        return this;
    }

    showUserProfilePage(userID) {
        $(this.$el).addClass(`${this.className()}--active`);
        this.$closeBtn.find('div').addClass('s1 s2 s3');

        this.addUserProfile(userID);
    }

    hideUserProfilePage() {
        $(this.$el).removeClass(`${this.className()}--active`);
        this.$closeBtn.find('div').removeClass('s1 s2 s3');

        this.$content.html('Loading...');
    }

    addUserProfile(userID) {
        const model = new UserModel({id: userID});
        model.fetch({
            success: (() => {
                const UserProfile = new UserProfileView({
                    model: model
                });

                this.$content.html( UserProfile.render().$el );
            }),
            error: ((e) => {
                console.log(' Service request failure ');
                this.$content.html('Something went wrong. Try later.');
            }),
            complete: ((e) => {
                console.log(' Service request completed ');
            })
        });
    }
}
