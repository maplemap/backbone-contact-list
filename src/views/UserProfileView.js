import Events from '../events';
import UserCollection from '../collections/UserCollection';
import UserAddressView from './UserAddressView';
import Templates from '../templates/';


export default class UserProfileView extends Backbone.View {
    tagName() { return "div"; }

    className() { return "user-profile";}

    events() {
        return {
            "click .editable": "showEditModalWindow",
        };
    }

    initialize() {
        Events.on('edited-profile-data', this.saveNewData, this);
        this.userAddressView = new UserAddressView({data: this.model.toJSON().address});
    }

    render() {
        this.$el.html( Templates.userProfile(this.model.toJSON()) );
        this.$el.find('[data-type="full-address"]').html( this.userAddressView.render().$el );

        return this;
    }

    showEditModalWindow(e) {
        const $textContainer = $(e.target);
        const dataType = $textContainer.data('type');
        const value = $textContainer.text();

        Events.trigger('show-edit-modal', {type: dataType, value: value});
    }

    saveNewData(data) {
        const newData = {
            id: this.model.toJSON().id
        };
        newData[data.type] = data.value;

        this.model.save(newData);

        this.render();
        this.updateUserCollection(newData);
    }

    updateUserCollection(newData) {
        const model = UserCollection.find((model) => model.get('id') === newData.id);
        model.set(newData);

        UserCollection.update(model);
    }
}
