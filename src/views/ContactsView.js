import Templates from '../templates';
import UsersListView from './UsersListView';
import UserProfileView from './UserProfilePageView';
import ModalEditView from './ModalEditView';

export default class ContactsView extends Backbone.View {
    tagName() { return "div"; }

    className() { return "contacts";}

    initialize() {
        this.usersListView = new UsersListView();
        this.userProfileView = new UserProfileView();
        this.ModalEditView = new ModalEditView();

        this.render();
    }

    render() {
        this.$el.append( Templates.listHead() );
        this.$el.append( this.usersListView.$el );
        this.$el.append( this.userProfileView.$el );
        this.$el.append( this.ModalEditView.$el );
    }
}
