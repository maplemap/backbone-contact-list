import Events from '../events';
import UserCollection from '../collections/UserCollection';
import UserItemView from './UserItemView';


export default class UsersListView extends Backbone.View {
    tagName() { return "div"; }

    className() { return "contacts__list";}

    initialize() {
        this.listenTo(UserCollection, 'add', this.addOne);
        this.listenTo(UserCollection, 'change', this.renderList);

        UserCollection.fetch();
        this.render();
    }

    render() {
        this.$el.html(  )
    }

    addOne(model) {
        var view = new UserItemView({
            model: model
        });

        this.$el.append( view.render().$el );
    }

    renderList() {
        this.$el.html('');

        UserCollection.each((model, indx) => {
            this.addOne(model);
        });
    }
}
