import Events from '../events';
import Templates from '../templates/';
import Validation from '../libs/validation';


export default class ModalWindowView extends Backbone.View {
    tagName() { return "div"; }

    className() { return "modal";}

    events() {
        return {
            "click .ok": "verifyData",
            "click .cancel": "hideEditModal",
            "focus .modal__edit-field": "clearErrors"
        };
    }

    initialize() {
        Events.on('show-edit-modal', this.showEditModal, this);

        this.render();
    }

    render() {
        this.$el.html( Templates.modalWindow() );
        this.$modalHeaderType = this.$el.find(`.${this.className()}__header-type`);
        this.$modalErrors = this.$el.find(`.${this.className()}__errors`);
        this.$modalEditField = this.$el.find(`.${this.className()}__edit-field`);

        return this;
    }

    showEditModal(data) {
        this.$modalHeaderType.data('type', data.type).text(data.type);
        this.$modalEditField.text(data.value);

        this.$el.addClass(`${this.className()}--active`);
    }

    verifyData() {
        const type = this.$modalHeaderType.data('type');
        const value = this.$modalEditField.text();

        const validation = Validation[type](value);

        if(validation.result === 'failed') {
            return this.$modalErrors.text(validation.description)
        } else {
            Events.trigger('edited-profile-data', {type: type, value: value})
            this.hideEditModal();
        }
    }

    hideEditModal() {
        this.data = null;
        this.$modalErrors.text('');
        this.$modalEditField.text('');


        this.$el.removeClass(`${this.className()}--active`);
    }

    clearErrors() {
        this.$modalErrors.text('');
    }
}
