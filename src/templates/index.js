export default {
    listHead() {
        return `
                <div class="contacts__head">
                    Contacts List
                </div>
               `
    },
    userListItem(data) {
        return `
                <div class="contact">
                  <img src="https://thebenclark.files.wordpress.com/2014/03/facebook-default-no-profile-pic.jpg" />
                  <p>
                    <strong>${data.name}</strong>
                    <span class="contact__email">${data.email}</span>
                    <span class="contact__city">${data.address.city}</span>
                  </p>
                </div>
              `
    },

    userProfilePage() {
        return `
                <div class="user-profile-page__close">
                    <div class="cy"></div>
                    <div class="cx"></div>
                </div>
                <div class="user-profile-page__content">Loading...</div>
               `
    },

    userProfile(data) {
        return `
                <div class="user-profile__img">
                  <img src="https://thebenclark.files.wordpress.com/2014/03/facebook-default-no-profile-pic.jpg" />
                </div>
                <div class="user-profile__name">${data.name}</div>
                <div data-type="username">${data.username}</div>
                <div class="editable" contenteditable data-type="email">${data.email}</div>
                <div data-type="full-address"></div>
                <div data-type="phone">${data.phone}</div>
                <div class="editable" contenteditable data-type="website">${data.website}</div>
               `
    },

    modalWindow(data) {
        return `
                <div class="modal__body">
                    <div class="modal__header">Edit <span class="modal__header-type"></span></div>
                    <div class="modal__errors"></div>
                    <div class="modal__edit-field" contenteditable></div>
                    <button class="btn ok">Ok</button>
                    <button class="btn cancel">Cancel</button>
                </div>
               `
    },

    userAddress(data) {
        return `
                <div data-type="city">${data.city}</div>
                <div data-type="geo">${data.geo.lat} ${data.geo.lng}</div>
                <div data-type="street">${data.street}</div>
                <div data-type="suite">${data.suite}</div>
                <div data-type="zipcode">${data.zipcode}</div>
               `
    }
}
