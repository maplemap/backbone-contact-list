import ContactModel from '../models/UserModel';
import {apiUrls} from '../config.json';


class UserCollection extends Backbone.Collection {
    get model() { return ContactModel; }
    get url() { return apiUrls.users; }

    update(model, callback) {
        this.set({model}, {add: false, remove: false})

        if(callback) callback;
    }
}

export default new UserCollection();
