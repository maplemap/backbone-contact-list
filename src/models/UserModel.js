import {apiUrls} from '../config.json';

export default class UserModel extends Backbone.Model {
    defaults() {
      return {
          name: '',
          username: '',
          address: '',
          email: '',
          website: ''
      }
    }

    get urlRoot() { return `${apiUrls.users}`; }
}
