const regExp = {
        mail: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        website: /^([a-zA-Z0-9]+(\.[a-zA-Z0-9]+)+.*)$/
    }

export default {

        email: function (email) {
            if( !email ) return {result: 'failed', description: 'Email can not be empty'};
            if( !regExp.mail.test(email) ) return {result: 'failed', description: 'Email must be valid'};

            return {result: 'success'}
        },

        website: function (phone) {
            if( !phone ) return {result: 'failed', description: 'Website can not be empty'};
            if( phone.length < 10 ) return {result: 'failed', description: 'Website must be valid'};

            return {result: 'success'}
        }
    };
