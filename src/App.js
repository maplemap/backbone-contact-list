import $ from 'jquery';
import Backbone from 'backbone';
import _ from 'underscore';

window.jQuery = $;
window.$ = $;
window.Backbone = Backbone;
window._ = _;

import './styles/main.less'
import ContactsView from './views/ContactsView';


$(window).on('load', () => {
    var contactsView = new ContactsView();
    $('#main').append( contactsView.$el );
});
